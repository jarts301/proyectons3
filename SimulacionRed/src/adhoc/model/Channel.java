package adhoc.model;

import adhoc.model.exceptions.ChannelCapacityExceededException;

public class Channel {

	public int capacity;
	public int numPackets;
	protected NetworkInterfaceSpeed interfaceSpeed;

	public NetworkInterfaceSpeed getInterfaceSpeed() {
		return interfaceSpeed;
	}

	public void setInterfaceSpeed(NetworkInterfaceSpeed interfaceSpeed) {
		this.interfaceSpeed = interfaceSpeed;
	}

	public Channel(NetworkInterfaceSpeed in) {
		this.interfaceSpeed = in;
		capacity = (int) (interfaceSpeed.speed() / Packet.SIZE);
		numPackets = 0;
	}

	public void addPackets(int num) {
		if (numPackets + num > capacity)
			throw new ChannelCapacityExceededException("The channel is at full capacity.");

		numPackets += num;

		if (numPackets < 0)
			numPackets = 0;
	}

	public float availableCapacity() {
		return (capacity - numPackets)* 100 / capacity;
	}

}