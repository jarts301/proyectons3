package adhoc.model.exceptions;


@SuppressWarnings("serial")
public class ChannelCapacityExceededException extends NetworkException {
	
	public ChannelCapacityExceededException(String message) {
        super(message);
    }

}
