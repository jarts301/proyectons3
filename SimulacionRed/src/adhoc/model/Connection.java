package adhoc.model;

import adhoc.model.exceptions.ChannelCapacityExceededException;

public class Connection {
	
	public void transmitData(Device source, Device target, Data data) {

		int idData = data.getId();

		if (stablishConnection(source, target)) {
			int speed = Math.min(source.channel.getInterfaceSpeed().speed(),
					target.channel.getInterfaceSpeed().speed());
			int timePerPacket = (int) (data.size() / speed);
			
			int numPackets = data.size();

			int sendedPackets = 0;

			for (sendedPackets = 0; sendedPackets < numPackets; sendedPackets++) {
				try {
					target.channel.addPackets(1);
					source.channel.addPackets(1);
					Thread.sleep(timePerPacket*100);
					target.channel.addPackets(-1);
					source.channel.addPackets(-1);
				} catch (ChannelCapacityExceededException e) {
					break;
				} catch (InterruptedException e) {
					e.printStackTrace();
					break;
				}
			}

			int lostPackets = numPackets - sendedPackets;

			if (source.outgoingConnections.containsKey(idData)) {
				Transmission tr = source.outgoingConnections.get(idData);
				int currentSended = tr.getNumPackets();
				int currentLost = tr.getLostPackets();
				tr.setNumPackets(currentSended + sendedPackets);
				tr.setLostPackets(currentLost + lostPackets);
			} else {
				source.outgoingConnections.put(idData, new Transmission(data, sendedPackets, lostPackets));
			}

			if (target.incomingConnections.containsKey(idData)) {
				Transmission tr = target.incomingConnections.get(idData);
				int currentSended = tr.getNumPackets();
				int currentLost = tr.getLostPackets();
				tr.setNumPackets(currentSended + sendedPackets);
				tr.setLostPackets(currentLost + lostPackets);
			} else {
				target.incomingConnections.put(idData, new Transmission(data, sendedPackets, lostPackets));
			}
		}
	}

	public boolean stablishConnection(Device source, Device target) {

		if (source.getAvailableDevices().contains(target)) {
			if (source.channel.availableCapacity() > 0 && target.channel.availableCapacity() > 0) {
				return true;
			}
		}

		return false;
	}
}