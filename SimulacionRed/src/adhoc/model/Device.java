package adhoc.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import unal.estocasticos.proyecto.Iconos;
import adhoc.util.Point2D;

public abstract class Device implements Runnable {

	protected Point2D speed;
	protected List<Data> dataRequired;
	protected List<Data> dataAssigned;
	protected List<Device> availableDevices;
	protected List<Connection> connections;
	protected Map<Integer, Transmission> incomingConnections;
	protected Map<Integer, Transmission> outgoingConnections;
	protected int range;
	protected Point2D position;
	protected String icon;

	protected Channel channel;
	protected int freeCapacity;

	protected String fileIcon;
	protected String musicIcon;
	protected String videoIcon;
	
	protected int[] dataReceived = new int[]{0,0,0};

	public Device(NetworkInterfaceSpeed interfaceSpeed, Point2D position) {
		this.channel = new Channel(interfaceSpeed);
		this.position = position;
		this.availableDevices = new ArrayList<Device>();
		
		this.fileIcon = Iconos.URL_FILE;
		this.musicIcon = Iconos.URL_MUSIC;
		this.videoIcon = Iconos.URL_VIDEO;
		this.incomingConnections = new HashMap<Integer, Transmission>();
		this.outgoingConnections = new HashMap<Integer, Transmission>();
	}
	
	public void run() {
        
		System.out.println("empieza");
		while(this.dataRequired.size() > 0) {
			
			for (Device d: availableDevices) {
				for (Data data : dataRequired){
					if(d.getDataAssigned().contains(data)){
						new Connection().transmitData(d, this, data);
					}
				}
				
				List<Data> dataAcquired = new ArrayList<Data>();
				for (Data data : dataRequired){
					if(incomingConnections.get(data.getId()) != null && incomingConnections.get(data.getId()).isCompleted()) {
						dataAcquired.add(data);
						System.out.println("dato " + data.getId() + " completo, tama�o " + data.size());
					}
				}
				
				dataRequired.removeAll(dataAcquired);
			}
		}
		
		System.out.println("Completo");
    }

	public Point2D getSpeed() {
		return speed;
	}
	public void setSpeed(Point2D speed) {
		this.speed = speed;
	}
	public List<Data> getDataRequired() {
		return dataRequired;
	}
	public void setDataRequired(List<Data> dataRequired) {
		this.dataRequired = dataRequired;
	}
	public Channel getChannel() {
		return channel;
	}
	public void setChannel(Channel channel) {
		this.channel = channel;
	}
	public int getRange() {
		return range;
	}
	public void setRange(int range) {
		this.range = range;
	}
	public Point2D getPosition() {
		return position;
	}
	public void setPosition(Point2D position) {
		this.position = position;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public List<Device> getAvailableDevices() {
		return availableDevices;
	}
	public void setAvailableDevices(List<Device> availableDevices) {
		this.availableDevices = availableDevices;
	}
	public List<Data> getDataAssigned() {
		return dataAssigned;
	}
	public void setDataAssigned(List<Data> dataAssigned) {
		this.dataAssigned = dataAssigned;
	}
	public int getFreeCapacity() {
		return freeCapacity;
	}
	public void setFreeCapacity(int freeCapacity) {
		this.freeCapacity = freeCapacity;
	}
	public String getFileIcon() {
		return fileIcon;
	}
	public void setFileIcon(String fileIcon) {
		this.fileIcon = fileIcon;
	}
	public String getMusicIcon() {
		return musicIcon;
	}
	public void setMusicIcon(String musicIcon) {
		this.musicIcon = musicIcon;
	}
	public String getVideoIcon() {
		return videoIcon;
	}
	public void setVideoIcon(String videoIcon) {
		this.videoIcon = videoIcon;
	}

	public Map<Integer, Transmission> getIncomingConnections() {
		return incomingConnections;
	}

	public void setIncomingConnections(
			Map<Integer, Transmission> incomingConnections) {
		this.incomingConnections = incomingConnections;
	}

	public Map<Integer, Transmission> getOutgoingConnections() {
		return outgoingConnections;
	}

	public void setOutgoingConnections(
			Map<Integer, Transmission> outgoingConnections) {
		this.outgoingConnections = outgoingConnections;
	}
	
}
