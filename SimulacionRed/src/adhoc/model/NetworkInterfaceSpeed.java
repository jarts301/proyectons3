package adhoc.model;

public enum NetworkInterfaceSpeed {

	S802_11g(54), S802_11n(300), S802_11ac(1000);

	private final int speed;

	NetworkInterfaceSpeed(int value) {
		this.speed = value;
	}

	int speed() {
		return speed;
	}

}