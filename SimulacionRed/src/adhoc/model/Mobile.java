package adhoc.model;

import unal.estocasticos.proyecto.Iconos;
import adhoc.util.Point2D;

public class Mobile extends Device {

	public static final int MAX_M_RANGE = 45;
	public static final int MIN_M_RANGE = 125;
	
	public static final int MAX_M_SPEED = 3;
	public static final int MIN_M_SPEED = 1;
	
	public Mobile(NetworkInterfaceSpeed interfaceSpeed, Point2D position) {
		super(interfaceSpeed, position);
		this.freeCapacity = 80;
		this.icon = Iconos.URL_MOBILE;
		this.speed = new Point2D((int)(Math.random()*MAX_M_SPEED) + MIN_M_SPEED,
								 (int)(Math.random()*MAX_M_SPEED) + MIN_M_SPEED);
		this.range = (int)(Math.random() * MAX_M_RANGE) + MIN_M_RANGE;
	}

}
