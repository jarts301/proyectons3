package adhoc.model;

import unal.estocasticos.proyecto.Iconos;
import adhoc.util.Point2D;

public class Desktop extends Device {
	
	public static final int MAX_DESKTOP_RANGE = 50;
	public static final int MIN_DESKTOP_RANGE = 180;

	public Desktop(NetworkInterfaceSpeed interfaceSpeed, Point2D position) {
		super(interfaceSpeed, position);
		this.freeCapacity = 10;
		this.icon = Iconos.URL_DESKTOP;
		this.speed = new Point2D(0, 0);
		this.range = (int)(Math.random() * MAX_DESKTOP_RANGE) + MIN_DESKTOP_RANGE;
	}

}
