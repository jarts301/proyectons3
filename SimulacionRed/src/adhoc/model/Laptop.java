package adhoc.model;

import unal.estocasticos.proyecto.Iconos;
import adhoc.util.Point2D;

public class Laptop extends Device {
	
	public static final int MAX_L_RANGE = 50;
	public static final int MIN_L_RANGE = 140;
	
	public static final int MAX_L_SPEED = 2;

	public Laptop(NetworkInterfaceSpeed interfaceSpeed, Point2D position) {
		super(interfaceSpeed, position);
		this.icon = Iconos.URL_LAPTOP;
		this.freeCapacity = 50;
		this.speed = new Point2D((int)(Math.random()*MAX_L_SPEED),
				                 (int)(Math.random()*MAX_L_SPEED));
		this.range = (int)(Math.random() * MAX_L_RANGE) + MIN_L_RANGE;
	}

}