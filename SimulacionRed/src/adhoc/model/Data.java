package adhoc.model;

public class Data {

	public enum TYPE{
		FILE, MUSIC, VIDEO
	}
	
	private int id;
	private int size;
	private TYPE type;

	public Data(int id, int size) {
		this.id = id;
		this.size = size;
		this.type = TYPE.FILE;
	}

	public int getId() {
		return id;
	}
	public int size() {
		return size;
	}
	public TYPE getType() {
		return type;
	}
	public void setType(TYPE type) {
		this.type = type;
	}

}