package adhoc.model;

public class Transmission {

	private Data data;
	private int numPackets;
	private int lostPackets;
	private long startTime;
	private long endTime;

	public Transmission(Data data) {
		this.data = data;
		this.startTime = System.nanoTime();
		this.numPackets = 0;
		this.lostPackets = 0;
	}

	public Transmission(Data data, int numPackets, int lostPackets) {
		this.data = data;
		this.startTime = System.nanoTime();
		this.numPackets = numPackets;
		this.lostPackets = lostPackets;
	}

	public int getNumPackets() {
		return numPackets;
	}

	public void setNumPackets(int numPackets) {
		this.numPackets = numPackets;
	}

	public int getLostPackets() {
		return lostPackets;
	}

	public void setLostPackets(int lostPackets) {
		this.lostPackets = lostPackets;
	}

	public boolean isCompleted() {
		if (numPackets >= data.size()) {
			endTime = System.nanoTime();
			return true;
		}

		return false;
	}

	public long getDuration() {
		return endTime - startTime;
	}

}