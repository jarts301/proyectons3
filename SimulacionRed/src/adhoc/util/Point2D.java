package adhoc.util;

public class Point2D {
	
	private int xCoord;
	private int yCoord;
	
	public Point2D(){}
	
	public Point2D(int xCoord, int yCoord){
		this.xCoord = xCoord;
		this.yCoord = yCoord;
	}
	
	public int getxCoord() {
		return xCoord;
	}
	public void setxCoord(int xCoord) {
		this.xCoord = xCoord;
	}
	public int getyCoord() {
		return yCoord;
	}
	public void setyCoord(int yCoord) {
		this.yCoord = yCoord;
	}
	
	public void revertX(){
		this.xCoord *= -1; 
	}
	
	public void revertY(){
		this.yCoord *= -1; 
	}
	
	public void randomOrigin(int xMax, int yMax){
		this.xCoord = (int) (Math.random() * xMax - 100) + 50;
		this.yCoord = (int) (Math.random() * yMax - 100) + 50;
	}
	
	public int getDistance(Point2D endPoint){
		int xDist = (int) Math.pow(this.xCoord - endPoint.xCoord, 2);
		int yDist = (int) Math.pow(this.yCoord - endPoint.yCoord, 2);
		int distance = (int)Math.round(Math.sqrt(xDist + yDist));
		return distance;
	}

}
