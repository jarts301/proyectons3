package adhoc.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adhoc.model.Device;
import unal.estocasticos.proyecto.SimulacionMain;

public class VisualData {

	public int formWidth;
	public int displayWidth;
	public int displayHeight;
	public SimulacionMain context;
	public List<Device> devices;
	public Map<Integer, String> data;

	public VisualData(SimulacionMain context, int formWidth, int displayWidth, int displayHeight,
			List<Device> devices) {
		this.context = context;
		this.formWidth = formWidth;
		this.displayWidth = displayWidth;
		this.displayHeight = displayHeight;
		this.devices = devices;
		this.data = new HashMap<Integer, String>();
		data.put(1, "P: 5, S: 5, r: 5, x:58");
		data.put(2, "P: 3, S: 23, r: 6, x:456");
	}

	public void draw() {
		context.fill(255, 255, 255, 254);
		context.stroke(255);
		context.rect(displayWidth, 0, displayWidth + formWidth, displayHeight);
		context.fill(0, 102, 153);
		
		context.textSize(18);
		context.text("DEVICES", displayWidth + 80, 35);
		context.textSize(13);
		for (Map.Entry<Integer, String> entry : data.entrySet()) {
			context.text("Device " + entry.getKey() + ": " + entry.getValue(), displayWidth + 5,
					50 + (20 * entry.getKey()));
		}
		/*for (int i=0; i<devices.size();i++) {
			context.text("Device " + (i+1) + ":" + devices.get(i).getDataAssigned().size(), displayWidth + 5, 60 + (20 * i));
		}*/
		context.textSize(9);
		context.fill(0, 0, 0);

	}

	public Map<Integer, String> getData() {
		return data;
	}

	public void setData(Map<Integer, String> data) {
		this.data = data;
	}

}
