package adhoc.util;

import adhoc.model.Device;

public class Joint2D {
	
	private Device startDevice;
	private Device endDevice;
	
	public Joint2D(Device startDevice, Device endDevice){
		this.startDevice = startDevice;
		this.endDevice = endDevice;
	}

	public Device getStartDevice() {
		return startDevice;
	}

	public void setStartDevice(Device startDevice) {
		this.startDevice = startDevice;
	}

	public Device getEndDevice() {
		return endDevice;
	}

	public void setEndDevice(Device endDevice) {
		this.endDevice = endDevice;
	}

	@Override
	public boolean equals(Object other){
		boolean startEqualsStart = startDevice.equals(((Joint2D)other).startDevice);
		boolean startEqualsEnd = startDevice.equals(((Joint2D)other).endDevice);
		if( startEqualsStart || startEqualsEnd ){
			boolean endEqualsStart = endDevice.equals(((Joint2D)other).startDevice);
			boolean endEqualsEnd = endDevice.equals(((Joint2D)other).endDevice);
			if( endEqualsStart || endEqualsEnd)
				return true;
		}
		return false;
	}

}
