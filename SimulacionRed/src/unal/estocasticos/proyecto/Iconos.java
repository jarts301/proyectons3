package unal.estocasticos.proyecto;

public class Iconos {
	
	public static final String URL_DESKTOP = "images/desktop2.png";
	public static final String URL_FILE = "images/file2.png";
	public static final String URL_LAPTOP = "images/laptop2.png";
	public static final String URL_MOBILE = "images/mobile2.png";
	public static final String URL_MUSIC = "images/music2.png";
	public static final String URL_VIDEO = "images/video2.png";
	
}
