package unal.estocasticos.proyecto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import adhoc.model.*;
import adhoc.model.Data.TYPE;
import adhoc.util.Joint2D;
import adhoc.util.Point2D;
import adhoc.util.VisualData;
import processing.core.PApplet;
import processing.core.PImage;
import processing.event.MouseEvent;

public class SimulacionMain extends PApplet {
	
	public final static int MAX_SIZE_DATA = 100;
	public final static double THRESHOLD_DATA_REQUIRED = 0.65;
	public final static double THRESHOLD_DATA_ASSIGNED = 0.35;
	
	public static final int CAPACITY_RADIUS = 15;
	public static final float PI = 3.1415f;

	private List<Device> devices;
	private VisualData visualData;
	private int displayWidth;
	private int formWidth;
	private int displayHeight;
	
	private int frameCount = 0;
	
	private int desktopsCount;
	private int laptopsCount;
	private int mobilesCount;

	/**
	 * Inicializa la lista de datos que se tienen y que se requieren en cada
	 * dispositivo. El tama�o de cada dato se establece de manera aleatoria con
	 * un tama�o maximo de MAX_SIZE_DATA
	 * 
	 * @param maxNumData:
	 *            Tama�o maximo de datos a generar
	 */
	public void initDevices(int maxNumData) {
		createDevices();
		ArrayList<Data> data = new ArrayList<Data>();

		for (int i = 0; i < maxNumData; i++)
			data.add(new Data(i, (int) (Math.random() * MAX_SIZE_DATA)));

		for (Device d : devices) {
			ArrayList<Data> dataRequired = new ArrayList<Data>();
			ArrayList<Data> dataAssigned = new ArrayList<Data>();
			for (int i = 0; i < (int)(Math.random() * data.size()); i++) {
				if (Math.random() > THRESHOLD_DATA_REQUIRED)
					dataRequired.add(data.get(i));

				if (Math.random() < THRESHOLD_DATA_ASSIGNED)
					dataAssigned.add(data.get(i));
			}
			d.setDataRequired(dataRequired);
			d.setDataAssigned(dataAssigned);
		}
	}
	
	public void createDevices(){
		desktopsCount = (int)(Math.random() * 3) + 1;
		laptopsCount = (int)(Math.random() * 3) + 1;
		mobilesCount = (int)(Math.random() * 5) + 4;
		for(int i = 0; i < desktopsCount; i++){
			Point2D startCoord = new Point2D();
			startCoord.randomOrigin(displayWidth, displayHeight);
			devices.add(new Desktop(NetworkInterfaceSpeed.S802_11ac, startCoord));
		}
		for(int i = 0; i < laptopsCount; i++){
			Point2D startCoord = new Point2D();
			startCoord.randomOrigin(displayWidth, displayHeight);
			devices.add(new Laptop(NetworkInterfaceSpeed.S802_11n, startCoord));
		}
		for(int i = 0; i < mobilesCount; i++){
			Point2D startCoord = new Point2D();
			startCoord.randomOrigin(displayWidth, displayHeight);
			devices.add(new Mobile(NetworkInterfaceSpeed.S802_11g, startCoord));
		}
	}

	@Override
	public void settings() {
		displayWidth = 900;
		displayHeight = 650;
		formWidth = 280;
		size(displayWidth+formWidth, displayHeight);
	}

	@Override
	public void setup() {
		//surface.setResizable(true);
		devices = new ArrayList<Device>();
		initDevices(20);//100
		visualData = new VisualData(this, formWidth, displayWidth, displayHeight,devices);
		System.out.println("init new transmission ");
		
		System.out.println(Arrays.toString(devices.get(0).getDataRequired().toArray()));
		System.out.println(Arrays.toString(devices.get(1).getDataAssigned().toArray()));
		for(Device device : devices)
			new Thread(device).start();
	}

	@Override
	public void draw() {
		background(255);
		incrementFrame();
		List<Joint2D> joints = new ArrayList<Joint2D>();
		for (Device device : devices) {
			int x = device.getPosition().getxCoord();
			int y = device.getPosition().getyCoord();
			
			getAvailableDevices(device);
			drawDevice(device, x, y);
			drawDeviceCapacity(device.getChannel().availableCapacity(), x, y);
			
			getSpeed(device);
			for (Device other : device.getAvailableDevices()) {
				Joint2D joint = new Joint2D(device, other);
				if (joints.indexOf(joint) == -1)
					joints.add(joint);
			}
			textSize(8);
			text("123", x + 15, y + 18);
			if (frameCount == 1)
				device.setPosition(new Point2D(x + device.getSpeed().getxCoord(), y + device.getSpeed().getyCoord()));
		}

		for (Joint2D joint : joints) {
			int startXCoord = joint.getStartDevice().getPosition().getxCoord();
			int startYCoord = joint.getStartDevice().getPosition().getyCoord();
			int endXCoord = joint.getEndDevice().getPosition().getxCoord();
			int endYCoord = joint.getEndDevice().getPosition().getyCoord();
			stroke(80, 80, 80);
			line(startXCoord, startYCoord, endXCoord, endYCoord);
		}
		
		visualData.draw();
	}
	
	public void drawDevice(Device device, int x, int y){
		PImage deviceImage = loadImage(device.getIcon());
		int xImage = x - deviceImage.width / 2;
		int yImage = y - deviceImage.height / 2;
		
		float radius = device.getRange();
		
		noFill();
		stroke(0);
		text(devices.indexOf(device), x - 1, y - 17);
		image(deviceImage, xImage, yImage);
		image(loadImage(device.getFileIcon()), x - 22, y + 25);
		image(loadImage(device.getMusicIcon()), x - 7, y + 25);
		image(loadImage(device.getVideoIcon()), x + 8, y + 25);
		
		if (device instanceof Mobile)  fill(20, 20, 250, 50);
		if (device instanceof Laptop)  fill(20, 250, 20, 50);
		if (device instanceof Desktop) fill(250, 250, 0, 50);
		
		noStroke();
		ellipse(x, y, radius, radius);
	}
	
	public void drawDeviceCapacity(float freeCapacity, int x, int y){
		strokeWeight(2);
		fill(250, 20, 20, 50);
		stroke(250, 20, 20, 150);
		arc(x, y, 50, 50, PI / 2, PI / 2 + 2 * (100-freeCapacity) * PI / 100);
		strokeWeight(1);
		fill(0, 0, 0);
		stroke(0);
	}

	public void incrementFrame() {
		frameCount++;
		if (frameCount > 1)
			frameCount = 0;
	}

	public void getAvailableDevices(Device device) {
		List<Device> availableDevices = new ArrayList<Device>();
		for (Device other : devices) {
			if (!other.equals(device)) {
				int range = device.getRange() / 2 + other.getRange() / 2;
				int distance = device.getPosition().getDistance(other.getPosition());
				if (distance < range && availableDevices.indexOf(other) == -1)
					availableDevices.add(other);
				if (distance >= range && availableDevices.indexOf(other) != -1)
					availableDevices.remove(other);
			}
		}
		device.setAvailableDevices(availableDevices);
	}

	public void getSpeed(Device device) {
		Point2D actualPos = device.getPosition();
		Point2D actualSpeed = device.getSpeed();
		
		if (actualPos.getxCoord() < 0 && actualSpeed.getxCoord() < 0)
			actualSpeed.revertX();

		if (actualPos.getxCoord() > displayWidth && actualSpeed.getxCoord() > 0)
			actualSpeed.revertX();

		if (actualPos.getyCoord() < 0 && actualSpeed.getyCoord() < 0)
			actualSpeed.revertY();

		if (actualPos.getyCoord() > displayHeight && actualSpeed.getyCoord() > 0)
			actualSpeed.revertY();

		device.setSpeed(actualSpeed);
	}

	@Override
	public void mouseWheel(MouseEvent event) {

	}

	public static void main(String[] args) {
		String name = SimulacionMain.class.getName();
		PApplet.main(name);
	}

}
